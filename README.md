### `composer require infotechnohelp/cakephp-seeds`

Terminal

`bin/cake plugin load Seeds`

`bin/cake migrations migrate --plugin Seeds`



#### `config/TrackedSeeds/_Queue.php`

```php
<?php

return  [
    'UserRoles',
    'Users',
];
```

#### `config/TrackedSeeds/UserRoles.php`

```php
<?php

$tableAlias = "AuthApi.UserRoles";

$data = [
    [
        'title' => 'User',
    ],
    [
        'title' => 'Admin',
    ],
    [
        'title' => 'Moderator',
    ],
];
```


#### `config/TrackedSeeds/Users.php`


```php
<?php

$tableAlias = "AuthApi.Users";

$data = [
    [
        'username' => 'Username',
        'email' => 'email1@email.com',
        'password' => '1234',
        'user_role_id' => 1,
    ],
    [
        'username' => 'Username 2',
        'email' => 'email2@email.com',
        'password' => '1234',
        'user_role_id' => 2,
    ],
    [
        'username' => 'Username 3',
        'email' => 'email3@email.com',
        'password' => '1234',
        'user_role_id' => 3,
    ],
];
```

Execute

`bin/cake seeds`