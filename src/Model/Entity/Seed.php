<?php

declare(strict_types=1);

namespace Seeds\Model\Entity;

use Cake\ORM\Entity;

/**
 * Seed Entity
 *
 * @property int $id
 * @property string $title
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Seeds\Model\Entity\Phinxlog[] $phinxlog
 */
class Seed extends Entity
{
    //@codingStandardsIgnoreStart
    protected $_accessible = [
        'title' => true,
        'created' => true,
        'modified' => true,
        'phinxlog' => true
    ];
    //@codingStandardsIgnoreEnd
}
