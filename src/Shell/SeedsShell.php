<?php

declare(strict_types=1);

namespace Seeds\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Seeds\Model\Table\SeedsTable;

/**
 * Class SeedsShell
 * @package Seeds\Shell
 */
class SeedsShell extends Shell
{
    public function main()
    {
        /** @var SeedsTable $SeedsTable */
        $SeedsTable = TableRegistry::getTableLocator()->get('Seeds.Seeds');

        $queue = require_once(CONFIG . 'TrackedSeeds' . DS . '_Queue.php');

        foreach ($queue as $seed) {
            if (!empty($SeedsTable->findByTitle($seed)->first())) {
                continue;
            }

            $count = 0;

            $tableAlias = null;
            $data = [];
            require_once(CONFIG . 'TrackedSeeds' . DS . $seed . '.php');

            $Table = TableRegistry::getTableLocator()->get($tableAlias);

            foreach ($data as $entityData) {
                $Table->saveOrFail($Table->newEntity($entityData));
                $count++;
            }

            $SeedsTable->saveOrFail($SeedsTable->newEntity(['title' => $seed]));
            $this->out(sprintf('%s: %s entities', $seed, $count));
        }
    }
}
